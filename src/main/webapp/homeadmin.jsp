<%@ page import="java.util.List" %>
<%@ page import="Entity.User" %>
<%@ page import="Entity.Role" %>
<%@ page import="Entity.User" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="myprefix" uri="/WEB-INF/userlist.tld" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<%
    String username = (String) request.getSession().getAttribute("username");

    out.print("<div class=\"adminname\" align=\"right\">Admin " + username + " (<a href=\"logout\">Logout</a>)");
    out.print("<div class=\"addref\" align=\"left\"><a href=\"/sendonaddchange?act=Add\">Add new user</a></div>");
%>

<div class="tableuser">
    <table border=\"1\" cellspacing=\"0\" cellpadding=\"5\">
        <tr>
            <td nowrap>Login</td>
            <td nowrap>First Name</td>
            <td nowrap>Last Name</td>
            <td>Age</td>
            <td>Role</td>
            <td nowrap>Actions</td>
        </tr>
        <myprefix:userlist/>
    </table>
</div>

<style type="text/css">
    .adminname {
        margin-right: 2%;
        margin-top: 2%;
    }

    .addref {
        float: left;
        margin-top: 10%;
        margin-left: 10%;
    }

    .tableuser {
        float: left;
        margin-top: 12%;
        margin-left: -6%;
    }
</style>

<dialog>
    <p>Are you sure?</p>
    <button id="yes">Yes</button>
    <button id="close">No</button>
</dialog>

<script>
    var dialog = document.querySelector('dialog');
    var login;
    var arr = document.querySelectorAll('.show');
    var logout = document.querySelector('.logout');var xhr = new XMLHttpRequest();

    arr.forEach(function (item) {
        item.onclick = function () {
            dialog.show();
            login = this.id;
            console.log(login);
        };
    });

    document.querySelector('#yes').onclick = function () {
        // document.location.href = 'sendindb?act=Del&login=' + login;
        xhr.open('DELETE', 'sendindb?act=Del&login=' + login);
        xhr.onload = function() {
            window.location.reload();
        };
        xhr.send();
    };


    document.querySelector('#close').onclick = function () {
        dialog.close(); // закрыть диалоговое окно
    };
</script>

<style type="text/css">
    dialog {
        border: 1px solid rgba(0, 0, 0, 0.3);
        border-radius: 6px;
        box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
    }
</style>

</body>
</html>
