<%@ page import="Entity.User" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<%
    String act = (String) request.getAttribute("act");
    String username = (String) request.getSession().getAttribute("username");
    User user = (User) request.getAttribute("user");
    out.print("<div align=\"right\">Admin " + username + " (<a href=\"logout\">Logout</a>)</div><p>");
    out.print(act + " user");
    out.print("<p>");
%>

<div class="userform">
    <%
        if (act.equals("Edit")) {
            out.println("<form nameRole=\"loginForm\" method=\"post\" action=\"sendindb?act=Edit\">");
            out.println("Username <input type=\"text\" name=\"username\" " +
                    "id=\"username\" class=\"field\" value=\"" + user.getLogin() + "\" readonly/> <br/>");
            out.println("<p>Password<input type=\"password\" name=\"password\" " +
                    "id=\"password\" class=\"field\" value=\"" + user.getPassword() + "\"/> <br/>");
            out.println("<p>Password again<input type=\"password\" name=\"passwordagain\" " +
                    "id=\"passwordagain\" class=\"field\" value=\"" + user.getPassword() + "\"/> <br/>");
            out.println("<p>Email<input type=\"text\" name=\"email\" " +
                    "id=\"email\" class=\"field\" value=\"" + user.getEmail() + "\"/> <br/>");
            out.println("<p>First name<input type=\"text\" name=\"firstname\" " +
                    "id=\"firstname\" class=\"field\" value=\"" + user.getFirstName() + "\"/> <br/>");
            out.println("<p>Last name<input type=\"text\" name=\"lastname\" " +
                    "id=\"lastname\" class=\"field\" value=\"" + user.getLastName() + "\"/> <br/>");
            out.println("<p>Birthday<input type=\"text\" name=\"birthday\" " +
                    "id=\"birthday\" class=\"field\" value=\"" + user.getBirthDay() + "\"/> <br/>");
            out.println("<p>Role<input type=\"text\" name=\"role\" " +
                    "id=\"role\" class=\"field\" value=\"" + user.getRole().getName() + "\" list=\"rolelist\"/>");
            out.println("<datalist id=\"rolelist\">\n" +
                    "                <option value=\"user\">\n" +
                    "                <option value=\"admin\">\n" +
                    "            </datalist>\n" +
                    "            <br/><p>");
        }
        if (act.equals("Add")) {
            out.println("<form nameRole=\"loginForm\" method=\"post\" action=\"sendindb?act=Add\">");
            out.println("Username <input type=\"text\" name=\"username\" " +
                    "id=\"username\" class=\"field\"/> <br/>");
            out.println("<p>Password<input type=\"password\" name=\"password\" " +
                    "id=\"password\" class=\"field\"/> <br/>");
            out.println("<p>Password again<input type=\"password\" name=\"passwordagain\" " +
                    "id=\"passwordagain\" class=\"field\"/> <br/>");
            out.println("<p>Email<input type=\"text\" name=\"email\" " +
                    "id=\"email\" class=\"field\"/> <br/>");
            out.println("<p>First name<input type=\"text\" name=\"firstname\" " +
                    "id=\"firstname\" class=\"field\"/> <br/>");
            out.println("<p>Last name<input type=\"text\" name=\"lastname\" " +
                    "id=\"lastname\" class=\"field\"/> <br/>");
            out.println("<p>Birthday<input type=\"text\" name=\"birthday\" " +
                    "id=\"birthday\" class=\"field\"/> <br/>");
            out.println("<p>Role<input type=\"text\" name=\"role\" " +
                    "id=\"role\" class=\"field\" list=\"rolelist\"/>");
            out.println("<datalist id=\"rolelist\">\n" +
                    "                <option value=\"user\">\n" +
                    "                <option value=\"admin\">\n" +
                    "            </datalist>\n" +
                    "            <br/><p>");
        }
        out.println("<input type=\"submit\" value=\"Ok\" id=\"submit\" onclick=\"location.href='/sendindb'\"/>");
        out.println("<input type=\"reset\" value=\"Cancel\" id=\"cancel\" onclick=\"location.href='/homeadmin.jsp'\"/>\n");
        out.println("</form>");
    %>
</div>

<style>
    .userform {
        position: absolute;
        width: 300px;
    }

    .field {
        float: right;
    }
</style>

</body>
</html>
