package DAO;

import Entity.Role;
import Entity.User;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.hibernate.Query;
import org.hibernate.Session;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import SessionFactory.HibernateSessionFactory;

import static DAO.JdbcRoleDao.getConnection;


public class JdbcUserDao extends AbstractjdbcDao implements UserDao {
    private Properties propertiesJdbc;
//    Properties propertiesLog = new Properties();
//    InputStream is = getClass().getResourceAsStream("/log4j.properties");

    static final Logger logger = Logger.getLogger(JdbcUserDao.class);

    public JdbcUserDao() {
//        PropertyConfigurator.configure("F:/JavaProjects/Servlet/src/main/resources/log4j.properties");

//        try {
//            propertiesLog.load(is);
//        } catch (IOException e) {
//            e.printStackTrace();
//        } finally {
//            try {
//                is.close();
//            }
//            catch (Exception e) {
//                // ignore this exception
//            }
//        }
//        PropertyConfigurator.configure(propertiesLog);
//        PropertyConfigurator.configure("Servlet/src/main/resources/log4j.properties");

    }

    @Override
    public final Connection createConnection() { // разобраться
        logger.info("Create connection");
        return getConnection(propertiesJdbc.getProperty("db.driver"),
                propertiesJdbc.getProperty("db.url"),
                propertiesJdbc.getProperty("db.username"),
                propertiesJdbc.getProperty("db.password"));
    }

    @Override
    public final void create(final User user) { // разобраться
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        session.beginTransaction();
        session.save(user);
        session.getTransaction().commit();
        session.close();
//        HibernateSessionFactory.shutdown();
    }

    @Override
    public final void update(final User user) {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        session.beginTransaction();
        session.update(user);
        session.getTransaction().commit();
        session.close();
//        HibernateSessionFactory.shutdown();
    }

    @Override
    public final void remove(final User user) {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        session.beginTransaction();
        session.delete(user);
        session.getTransaction().commit();
        session.close();
//        HibernateSessionFactory.shutdown();
    }

    @Override
    public final List<User> findAll() {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        session.beginTransaction();
        Query queryResult = session.createQuery("from User");
        List allUsers = queryResult.list();
        session.getTransaction().commit();
        session.close();
//        HibernateSessionFactory.shutdown();
        return allUsers;
    }

    @Override
    public final User findByLogin(final String login) {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        session.beginTransaction();
        User user = null;
        Query queryResult = session.createQuery("from User where login = '" + login + "'");
        List listUsers = queryResult.list();
        if (listUsers.size() > 0){
            user = (User) listUsers.get(0);
        }
        session.getTransaction().commit();
        session.close();
//        HibernateSessionFactory.shutdown();
        return user;
    }

    @Override
    public final User findByEmail(final String email) {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        session.beginTransaction();
        User user = null;
        Query queryResult = session.createQuery("from User where email = '" + email + "'");
        List listUsers = queryResult.list();
        if (listUsers.size() > 0){
            user = (User) listUsers.get(0);
        }
        session.getTransaction().commit();
        session.close();
//        HibernateSessionFactory.shutdown();
        return user;
    }


    public final void ch() {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        session.beginTransaction();
        session.createSQLQuery("ALTER TABLE ROLE ADD FOREIGN KEY (IDROLE) REFERENCES ROLE (IDROLE);");
        session.getTransaction().commit();
        session.close();
    }

//    public final void ch() {
//        try (Connection connection = createConnection();
//             Statement statement = connection.createStatement()) {
//            statement.execute("ALTER TABLE ROLE ALTER COLUMN ID BIGINT NOT NULL AUTO_INCREMENT;");
//        } catch (SQLException e) {
//            logger.error(e.getMessage());
//        }
//    }

//    public void createTable() {
//        try (Connection connection = createConnection();
//             Statement statement = connection.createStatement()) {
//            statement.execute("CREATE TABLE USER\n" +
//                    "(\n" +
//                    "    ID BIGINT PRIMARY KEY,\n" +
//                    "    LOGIN VARCHAR(255),\n" +
//                    "    PASSWORD VARCHAR(255),\n" +
//                    "    EMAIL VARCHAR(255),\n" +
//                    "    FIRSTNAME VARCHAR(255),\n" +
//                    "    LASTNAME VARCHAR(255),\n" +
//                    "    BIRTHDAY VARCHAR(255)\n" +
//                    ");");
//            statement.execute("    CREATE TABLE ROLE\n" +
//                    "            (\n" +
//                    "                    ID BIGINT PRIMARY KEY,\n" +
//                    "                    NAME VARCHAR(255),\n" +
//                    ");");
//        } catch (SQLException e) {
//            logger.error(e.getMessage());
//        }
//    }

}
