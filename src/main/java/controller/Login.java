package controller;

import DAO.*;
import Entity.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/login")
public class Login extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final String username = req.getParameter("username");
        final String password = req.getParameter("password");

        JdbcUserDao jdbcUserDao = new JdbcUserDao();
        User user = jdbcUserDao.findByLogin(username);
        if (user.getLogin().isEmpty() || !user.getPassword().equals(password)) {//проверки на нулл
            resp.sendRedirect("/login.jsp");
            return;
        }
        req.getSession().setAttribute("username", username);
        if (user.getRole().getName().equals("user")) {
            req.getRequestDispatcher("/homeuser.jsp").forward(req, resp);
            return;
        }
        if (user.getRole().getName().equals("admin")) {
            req.getRequestDispatcher("/homeadmin.jsp").forward(req, resp);
            return;
        }
        resp.sendRedirect("/login.jsp");
    }
}
