package controller;

import DAO.JdbcRoleDao;
import DAO.JdbcUserDao;
import Entity.Role;
import Entity.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;

@WebServlet("/sendindb")
public class UserManagerServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User user = new User();
        Role role = new Role();
        user.setRole(role);
        JdbcUserDao jdbcUserDao = new JdbcUserDao();
        JdbcRoleDao jdbcRoleDao = new JdbcRoleDao();
        final String act = req.getParameter("act");
        User userchack = jdbcUserDao.findByLogin(req.getParameter("username"));

        initUser(user, req, role, jdbcRoleDao);

        if (!req.getParameter("password").equals(req.getParameter("passwordagain")) ||
                !isDateValid(req.getParameter("birthday"))) {
            req.setAttribute("act", act);
            req.setAttribute("user", user);
            req.getRequestDispatcher("/addchangeuser.jsp").forward(req, resp);
            return;
        }

        if (act.equals("Edit")) {
            user.setId(userchack.getId());
            jdbcUserDao.update(user);
            resp.sendRedirect("/homeadmin.jsp");
        }
        if (act.equals("Add")) {
            if (userchack == null) {
                jdbcUserDao.create(user);
                resp.sendRedirect("/homeadmin.jsp");
                return;
            }
            resp.sendRedirect("/sendonaddchange?act=Add");
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("login");
        String act = req.getParameter("act");
        JdbcUserDao jdbcUserDao = new JdbcUserDao();
        User user = jdbcUserDao.findByLogin(login);
        if (act.equals("Del")) {
            jdbcUserDao.remove(user);
        }
    }

    public static boolean isDateValid(String date) {
        SimpleDateFormat myFormat = new SimpleDateFormat("dd.MM.yyyy");
        myFormat.setLenient(false);
        try {
            myFormat.parse(date);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private void initUser(User user, HttpServletRequest req, Role role, JdbcRoleDao jdbcRoleDao) {
        user.setLogin(req.getParameter("username"));
        user.setPassword(req.getParameter("password"));
        user.setEmail(req.getParameter("email"));
        user.setFirstName(req.getParameter("firstname"));
        user.setLastName(req.getParameter("lastname"));
        user.setBirthDay(req.getParameter("birthday"));
        role = jdbcRoleDao.findByName(req.getParameter("role"));
        user.setRole(role);
    }
}
