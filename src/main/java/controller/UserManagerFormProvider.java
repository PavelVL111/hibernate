package controller;

import DAO.JdbcUserDao;
import Entity.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/sendonaddchange")
public class UserManagerFormProvider extends HttpServlet {
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final String act = req.getParameter("act");
        final String login = req.getParameter("login");
        if (act.equals("Add")) {
            req.setAttribute("act", act);
            req.getRequestDispatcher("/addchangeuser.jsp").forward(req, resp);
        }
        if (act.equals("Edit")) {
            JdbcUserDao jdbcUserDao = new JdbcUserDao();
            User user = jdbcUserDao.findByLogin(login);
            req.setAttribute("act", act);
            req.setAttribute("user", user);
            req.getRequestDispatcher("/addchangeuser.jsp").forward(req, resp);
        }
    }
}
