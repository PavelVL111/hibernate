package Entity;

import javax.persistence.*;

import static javax.persistence.GenerationType.AUTO;
import static javax.persistence.GenerationType.IDENTITY;


@Entity
@Table(name = "USER", schema = "", catalog = "")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false, insertable = true, updatable = true)
    private Long id;

    @Basic
    @Column(name = "LOGIN", nullable = false, insertable = true, updatable = true, length = 255)
    private String login;

    @Basic
    @Column(name = "PASSWORD", nullable = false, insertable = true, updatable = true, length = 255)
    private String password;

    @Basic
    @Column(name = "EMAIL", nullable = false, insertable = true, updatable = true, length = 255)
    private String email;

    @Basic
    @Column(name = "FIRSTNAME", nullable = false, insertable = true, updatable = true, length = 255)
    private String firstName;

    @Basic
    @Column(name = "LASTNAME", nullable = false, insertable = true, updatable = true, length = 255)
    private String lastName;

    @Basic
    @Column(name = "BIRTHDAY", nullable = false, insertable = true, updatable = true, length = 255)
    private String birthDay;

    @ManyToOne()
    @JoinColumn(name = "IDROLE")
    private Role role;

    public final Long getId() {
        return id;
    }

    public final void setId(final Long id) {
        this.id = id;
    }

    public final String getLogin() {
        return login;
    }

    public final void setLogin(final String login) {
        this.login = login;
    }

    public final String getPassword() {
        return password;
    }

    public final void setPassword(final String password) {
        this.password = password;
    }

    public final String getEmail() {
        return email;
    }

    public final void setEmail(final String emailfirstName) {
        this.email = emailfirstName;
    }

    public final String getFirstName() {
        return firstName;
    }

    public final void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    public final String getLastName() {
        return lastName;
    }

    public final void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    public final String getBirthDay() {
        return birthDay;
    }

    public final void setBirthDay(final String birthDay) {
        this.birthDay = birthDay;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}
