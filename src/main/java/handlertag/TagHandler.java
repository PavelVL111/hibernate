package handlertag;

import DAO.JdbcUserDao;
import Entity.User;

import javax.servlet.jsp.*;
import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import javax.servlet.jsp.tagext.SimpleTagSupport;

public class TagHandler extends SimpleTagSupport {
    public void doTag() throws JspException, IOException {
        JdbcUserDao jdbcUserDao = new JdbcUserDao();
        List<User> list = jdbcUserDao.findAll();
        JspWriter out = getJspContext().getOut();

        for (User user : list) {
            out.print("<tr><td nowrap>" + user.getLogin() +
                    "</td><td nowrap>" + user.getFirstName() +
                    "</td><td nowrap>" + user.getLastName() +
                    "</td><td>" + getAge(user.getBirthDay()) +
                    "</td><td>" + user.getRole().getName() +
                    "</td><td nowrap><a href=\"sendonaddchange?login=" +
                    user.getLogin() + "&act=Edit\">Edit</a> " +
                    "<a href=\"#\" id=\"" +
                    user.getLogin() + "\" name=\"show\" class=\"show\" value=\"/login=" +
                    user.getLogin() + "\" onclick=\"return false;\">Delete</a></td></tr>");
        }
    }

    public static Integer getAge(String strBirthday)
    {
        SimpleDateFormat oldDateFormat = new SimpleDateFormat("dd.mm.yyyy", Locale.getDefault());
        Date birthday = null;
        try {
            birthday = oldDateFormat.parse(strBirthday);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();
        dob.setTime(birthday);
        dob.add(Calendar.DAY_OF_MONTH, -1);

        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
        if (today.get(Calendar.DAY_OF_YEAR) <= dob.get(Calendar.DAY_OF_YEAR)) {
            age--;
        }
        return age;
    }
}
