package DAO;

import Entity.User;
import org.dbunit.Assertion;
import org.dbunit.DBTestCase;
import org.dbunit.PropertiesBasedJdbcDatabaseTester;
import org.dbunit.dataset.Column;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.filter.DefaultColumnFilter;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Properties;

public class JdbcUserDaoTest extends DBTestCase {
    Properties properties;
    User user = new User();
    User userUpdate = new User();
    User userRemove = new User();
    JdbcUserDao jud = new JdbcUserDao();

    public JdbcUserDaoTest() {
        properties = new Properties();
        try {
            properties.load(Thread.currentThread()
                    .getContextClassLoader().getResourceAsStream("jdbc.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_DRIVER_CLASS, properties.getProperty("db.driver"));
        System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_CONNECTION_URL, properties.getProperty("db.url"));
        System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_USERNAME, properties.getProperty("db.username"));
        System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_PASSWORD, properties.getProperty("db.password"));

        user.setId(4L);
        user.setLogin("goshak");
        user.setPassword("dfr122");
        user.setEmail("@gosha");
        user.setFirstName("gosha");
        user.setLastName("petrov");
        user.setBirthDay("12.03.1990");

        userUpdate.setId(3L);
        userUpdate.setLogin("Jor");
        userUpdate.setPassword("HKL789");
        userUpdate.setEmail("@jora11");
        userUpdate.setFirstName("Jora");
        userUpdate.setLastName("Ivanov");
        userUpdate.setBirthDay("07.05.1985");

        userRemove.setId(3L);
        userRemove.setLogin("joraru");
        userRemove.setPassword("HKL789");
        userRemove.setEmail("@jora11");
        userRemove.setFirstName("Jora");
        userRemove.setLastName("Ivanov");
        userRemove.setBirthDay("07.05.1985");
        createTable();
    }
//___________
//    public void createTable() {
//        try (Connection connection = createConnection();
//             Statement statement = connection.createStatement()) {
//            statement.execute("CREATE TABLE USER\n" +
//                    "(\n" +
//                    "    ID BIGINT PRIMARY KEY,\n" +
//                    "    LOGIN VARCHAR(255),\n" +
//                    "    PASSWORD VARCHAR(255),\n" +
//                    "    EMAIL VARCHAR(255),\n" +
//                    "    FIRSTNAME VARCHAR(255),\n" +
//                    "    LASTNAME VARCHAR(255),\n" +
//                    "    BIRTHDAY VARCHAR(255)\n" +
//                    ");");
//            System.out.print("");
//        } catch (SQLException e) {
//            e.printStackTrace();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    public Connection createConnection() {
//        try {
//            properties.load(Thread.currentThread()
//                    .getContextClassLoader().getResourceAsStream("DAO.properties"));
//            Class.forName(properties.getProperty("db.driver"));
//            return DriverManager.getConnection(properties.getProperty("db.url"), properties.getProperty("db.username"), properties.getProperty("db.password"));
//        } catch (SQLException | ClassNotFoundException e) {
//            e.printStackTrace();
//            throw new RuntimeException();
//        } catch (IOException e) {
//            e.printStackTrace();
//            throw new RuntimeException();
//        }
//    }
//
//    @Before
//    public void initDB() throws SQLException {
//        IDatabaseConnection connection = null;
//        try {
//            connection = getConnection();
//            IDataSet dataSet = new FlatXmlDataSet(new FileInputStream("UserTestDataSet.xml"));
//            DatabaseOperation.CLEAN_INSERT.execute(connection, dataSet);
//        } catch (DatabaseUnitException e) {
//            e.printStackTrace();
//        } catch (SQLException e) {
//            e.printStackTrace();
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            connection.close();
//        }
//    }
    //________________

    @Override
    protected IDataSet getDataSet() throws Exception {
        return new FlatXmlDataSet(getClass().getResourceAsStream("/UserTestDataSet.xml"));
    }

    public void testCreateUser() throws Exception {
//        createTable();
        jud.create(user);

        IDataSet databaseDataSet = getConnection().createDataSet();
        ITable actualTable = databaseDataSet.getTable("USER");

        IDataSet expectedDataSet = new FlatXmlDataSet(getClass().getResourceAsStream("/UserTestInsert.xml"));

        ITable expectedTable = expectedDataSet.getTable("USER");
        ITable filteredActualTable = DefaultColumnFilter.includedColumnsTable(actualTable, expectedTable.getTableMetaData().getColumns());

        Assertion.assertEquals(expectedTable, filteredActualTable);
    }

    public void testUpdateUser() throws Exception {

        jud.update(userUpdate);

        IDataSet databaseDataSet = getConnection().createDataSet();
        ITable actualTable = databaseDataSet.getTable("USER");

        IDataSet expectedDataSet = new FlatXmlDataSet(getClass().getResourceAsStream("/UserTestUpdate.xml"));

        ITable expectedTable = expectedDataSet.getTable("USER");
        ITable filteredActualTable = DefaultColumnFilter.includedColumnsTable(actualTable, expectedTable.getTableMetaData().getColumns());

        Assertion.assertEquals(expectedTable, filteredActualTable);
    }

    public void testRemoveUser() throws Exception {
        jud.remove(userRemove);

        IDataSet databaseDataSet = getConnection().createDataSet();
        ITable actualTable = databaseDataSet.getTable("USER");

        IDataSet expectedDataSet = new FlatXmlDataSet(getClass().getResourceAsStream("/UserTestRemove.xml"));

        ITable expectedTable = expectedDataSet.getTable("USER");
        ITable filteredActualTable = DefaultColumnFilter.includedColumnsTable(actualTable, expectedTable.getTableMetaData().getColumns());

        Assertion.assertEquals(expectedTable, filteredActualTable);
    }

    public void testFindAllUser() throws Exception {
        List<User> users = jud.findAll();
        IDataSet expectedDataSet = new FlatXmlDataSet(getClass().getResourceAsStream("/UserTestDataSet.xml"));
        ITable expectedTable = expectedDataSet.getTable("USER");
        Column[] expectedColumn = expectedTable.getTableMetaData().getColumns();
        for (int i = 0; i < expectedTable.getRowCount(); i++) {
            assertTrue(users.get(i).getId().toString().equals((String) expectedTable.getValue(i, expectedColumn[0].getColumnName())));
            assertTrue(users.get(i).getLogin().equals((String) expectedTable.getValue(i, expectedColumn[1].getColumnName())));
            assertTrue(users.get(i).getPassword().equals((String) expectedTable.getValue(i, expectedColumn[2].getColumnName())));
            assertTrue(users.get(i).getEmail().equals((String) expectedTable.getValue(i, expectedColumn[3].getColumnName())));
            assertTrue(users.get(i).getFirstName().equals((String) expectedTable.getValue(i, expectedColumn[4].getColumnName())));
            assertTrue(users.get(i).getLastName().equals((String) expectedTable.getValue(i, expectedColumn[5].getColumnName())));
            assertTrue(users.get(i).getBirthDay().equals((String) expectedTable.getValue(i, expectedColumn[6].getColumnName())));
        }
    }


    public void testFindByLoginUser() throws Exception {
        User user = jud.findByLogin("sararou");
        assertTrue(user.getId() == 1);
        assertTrue(user.getLogin().equals("sararou"));
        assertTrue(user.getPassword().equals("ABC123"));
        assertTrue(user.getEmail().equals("@sara12"));
        assertTrue(user.getFirstName().equals("Sara"));
        assertTrue(user.getLastName().equals("Iphimova"));
        assertTrue(user.getBirthDay().equals("13.02.1991"));
    }

    public void testFindByEmailUser() throws Exception {
        User user = jud.findByEmail("@vasa45");
        assertTrue(user.getId() == 2);
        assertTrue(user.getLogin().equals("vasan"));
        assertTrue(user.getPassword().equals("XYZ456"));
        assertTrue(user.getEmail().equals("@vasa45"));
        assertTrue(user.getFirstName().equals("Vasa"));
        assertTrue(user.getLastName().equals("Kurchin"));
        assertTrue(user.getBirthDay().equals("02.03.1990"));
    }

    @Override
    protected DatabaseOperation getTearDownOperation() throws Exception {
        return DatabaseOperation.DELETE;
    }

    void createTable() {

        try (Connection connection = jud.createConnection();
             Statement statement = connection.createStatement()) {
            statement.execute("CREATE TABLE IF NOT EXISTS USER(ID BIGINT primary key, LOGIN varchar(64), PASSWORD varchar(64), EMAIL varchar(64), FIRSTNAME varchar(64), LASTNAME varchar(64), BIRTHDAY varchar(64));");
            statement.execute("CREATE TABLE IF NOT EXISTS ROLE(ID BIGINT primary key, NAME varchar(64));");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}