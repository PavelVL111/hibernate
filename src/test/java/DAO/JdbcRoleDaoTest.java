package DAO;

import Entity.Role;
import org.dbunit.Assertion;
import org.dbunit.DBTestCase;
import org.dbunit.PropertiesBasedJdbcDatabaseTester;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.filter.DefaultColumnFilter;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class JdbcRoleDaoTest extends DBTestCase {
    Properties properties;
    Role role = new Role();
    Role roleUpdate = new Role();
    Role roleRemove = new Role();
    JdbcRoleDao jrd = new JdbcRoleDao();

    public JdbcRoleDaoTest() {
        properties = new Properties();
        try {
            properties.load(Thread.currentThread()
                    .getContextClassLoader().getResourceAsStream("jdbc.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_DRIVER_CLASS, properties.getProperty("db.driver"));
        System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_CONNECTION_URL, properties.getProperty("db.url"));
        System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_USERNAME, properties.getProperty("db.username"));
        System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_PASSWORD, properties.getProperty("db.password"));
//        System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_DRIVER_CLASS, "org.h2.Driver");
//        System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_CONNECTION_URL, "DAO:h2:mem:test;DB_CLOSE_DELAY=-1");
//        System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_USERNAME, "sa");
//        System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_PASSWORD, "1234");
//        System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_SCHEMA, "");
        role.setId(4L);
        role.setName("nAme4");

        roleUpdate.setId(3L);
        roleUpdate.setName("nAme4");

        roleRemove.setId(3L);
        roleRemove.setName("nAme3");

        createTable();
    }

    @Override
    protected IDataSet getDataSet() throws Exception {
        return new FlatXmlDataSet(getClass().getResourceAsStream("/UserTestDataSet.xml"));
    }

    public void testCreateRole() throws Exception {
        jrd.create(role);
        IDataSet databaseDataSet = getConnection().createDataSet();
        ITable actualTable = databaseDataSet.getTable("ROLE");
        IDataSet expectedDataSet = new FlatXmlDataSet(getClass().getResourceAsStream("/UserTestInsert.xml"));
        ITable expectedTable = expectedDataSet.getTable("ROLE");
        ITable filteredActualTable = DefaultColumnFilter.includedColumnsTable(actualTable, expectedTable.getTableMetaData().getColumns());
        Assertion.assertEquals(expectedTable, filteredActualTable);
    }

    public void testRemoveRole() throws Exception {
        jrd.remove(roleRemove);
        IDataSet databaseDataSet = getConnection().createDataSet();
        ITable actualTable = databaseDataSet.getTable("ROLE");
        IDataSet expectedDataSet = new FlatXmlDataSet(getClass().getResourceAsStream("/UserTestRemove.xml"));
        ITable expectedTable = expectedDataSet.getTable("ROLE");
        ITable filteredActualTable = DefaultColumnFilter.includedColumnsTable(actualTable, expectedTable.getTableMetaData().getColumns());
        Assertion.assertEquals(expectedTable, filteredActualTable);
    }

    public void testUpdateRole() throws Exception {
        jrd.update(roleUpdate);
        IDataSet databaseDataSet = getConnection().createDataSet();
        ITable actualTable = databaseDataSet.getTable("ROLE");
        IDataSet expectedDataSet = new FlatXmlDataSet(getClass().getResourceAsStream("/UserTestUpdate.xml"));
        ITable expectedTable = expectedDataSet.getTable("ROLE");
        ITable filteredActualTable = DefaultColumnFilter.includedColumnsTable(actualTable, expectedTable.getTableMetaData().getColumns());
        Assertion.assertEquals(expectedTable, filteredActualTable);
    }

    public void testFindByNameRole() throws Exception {
        Role role = jrd.findByName("name1");
        assertTrue(role.getId() == 1);
        assertTrue(role.getName().equals("name1"));
    }

    @Override
    protected DatabaseOperation getTearDownOperation() throws Exception {
        return DatabaseOperation.DELETE;
    }

    void createTable() {
        try (Connection connection = jrd.createConnection();
             Statement statement = connection.createStatement()) {
            statement.execute("CREATE TABLE IF NOT EXISTS USER(ID BIGINT primary key, LOGIN varchar(64), PASSWORD varchar(64), EMAIL varchar(64), FIRSTNAME varchar(64), LASTNAME varchar(64), BIRTHDAY varchar(64));");
            statement.execute("CREATE TABLE IF NOT EXISTS ROLE(ID BIGINT primary key, NAME varchar(64));");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
